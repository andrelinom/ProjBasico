package br.com.funcoesLambdas.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import br.com.funcoesLambdas.models.Dependentes;
import br.com.funcoesLambdas.models.Pessoa;

public class MainClassesCollectionsComLista {
	public static void main(String[] args) {
		List<Pessoa> pessoas = novaLista();
		List<Dependentes> dependentes = listaDependentes();

		System.out.println("Lista");
		System.out.println("");
		pessoas.forEach(p -> System.out.println(p.toString()));
		System.out.println("----------------------------------------");		
		
		// Ordenar lista dependentes
		System.out.println("ordenar lista de dependentes");
		System.out.println("");
		pessoas.forEach(p -> {Collections.sort(p.getListaDependentes(), (Dependentes d1, Dependentes d2) -> d1.getNome().compareTo(d2.getNome()));
						  	   System.out.println(p.toString());
							});
		System.out.println("----------------------------------------");
		
		// Filtro equals lista
		pessoas = novaLista();
		System.out.println("Filtrar equals Ana 8  equivale ao Sql (campo) = 'Ana 8' da lista de dependentes");
		System.out.println("");
		
		pessoas.forEach(p -> {p.setListaDependentes( p.getListaDependentes().stream()
														.filter(s -> s.getNome().equals("Ana 8"))
														.collect(Collectors.toList()));	
								System.out.println(p.toString());
		});
		System.out.println("----------------------------------------");
		
		// Filtro startsWith
		pessoas = novaLista();
		System.out.println("Filtrar startsWith A equivale ao Sql campo like 'A%'  nas duas listas pai e filho");
		System.out.println("");
		
		pessoas.stream()
					.filter(s -> s.getNome()
					.toUpperCase()
					.startsWith("A"))
					.collect(Collectors.toList())		
					.forEach(p ->{ p.setListaDependentes( p.getListaDependentes().stream()
											.filter(s -> s.getNome()
														 .toUpperCase()
														 .startsWith("A"))											
											.collect(Collectors.toList()));
									System.out.println(p.toString());
								});
			
		System.out.println("----------------------------------------");
	
		// Filtro endsWith
		pessoas = novaLista();
		System.out.println("Filtrar endsWith a equivale ao Sql campo like '%a' nas duas listas pai e filho");
		System.out.println("");
		pessoas.stream()
					.filter(s -> s.getNome()
					.toUpperCase()
					.endsWith("A"))
					.collect(Collectors.toList())		
					.forEach(p ->{ p.setListaDependentes( p.getListaDependentes().stream()
											.filter(s -> s.getNome()
														 .toUpperCase()
														 .endsWith("A"))											
											.collect(Collectors.toList()));
									System.out.println(p.toString());
								});
		System.out.println("----------------------------------------");

		// Filtro split
		pessoas = novaLista();
		System.out.println("Filtrar em Regex a equivale ao Sql campo like '%an%'");
		System.out.println("");
		pessoas.stream()
				.filter(s -> s.getNome()
				.toUpperCase()
				.contains("AN"))
				.collect(Collectors.toList())		
				.forEach(p ->{ p.setListaDependentes( p.getListaDependentes().stream()
										.filter(s -> s.getNome()
													 .toUpperCase()
													 .contains("AN"))											
										.collect(Collectors.toList()));
								System.out.println(p.toString());
							});
		System.out.println("----------------------------------------");
		
		System.out.println("");
		
		//Colocar dependente apenas se a Pessoa for Andre e o dependente for Ana
		pessoas = listaPessoas();
		
		pessoas.stream()
				.filter(p -> p.getNome().equals("Andre"))
				.forEach(p -> p.setListaDependentes(dependentes.stream()
																.filter(dependente -> dependente.getNome().equals("Ana"))
																.collect(Collectors.toList())));
		System.out.println(pessoas);
		
		
		//Mesma coisa que o exemplo acima, por�m agora criando uma lista nova apenas com os casos afetados.
		pessoas.stream()
				.filter(pessoa -> pessoa.getNome().equals("Andre"))
				.forEach(pessoa -> pessoa.setListaDependentes(dependentes.stream()
																			.filter(d -> d.getNome().equals("Ana"))
																			.collect(Collectors.toList())));
		List<Pessoa> novaListaPessoas = 
				pessoas.stream()
						.filter(pessoa -> pessoa.getNome().equals("Andre"))
						.collect(Collectors.toList());
		
		System.out.println("");
		System.out.println(novaListaPessoas);
		
		//Trabalhando com listas grandes, utilizando programa��o paralela (ideal para listas realmente grandes)
		pessoas = listaPessoas();
		pessoas.parallelStream()
			.filter(p -> p.getNome().equals("Andre"))
			.forEach(p ->
				p.setListaDependentes
					(
						dependentes.stream()
							.filter(dependente -> dependente.getNome().equals("Ana"))
							.collect(Collectors.toList())
					)
			);
		
		System.out.println();
		System.out.println(pessoas);
	}

	private static List<Pessoa> novaLista() {
		// Carga de Dados pessoas
		List<Pessoa> pessoas = new ArrayList<>();
		List<Dependentes> listaDep = new ArrayList<>();
		
		listaDep.add(new Dependentes("Ana"));
		listaDep.add(new Dependentes("Kenia"));
		listaDep.add(new Dependentes("Claudia"));
		listaDep.add(new Dependentes("Bruna"));
		listaDep.add(new Dependentes("Antonia"));
		listaDep.add(new Dependentes("Fernanda"));
		listaDep.add(new Dependentes("Ana Carolina"));
		
		
		pessoas.add(new Pessoa("Andre", listaDep));		
		pessoas.add(new Pessoa("Douglas", listaDep));
		pessoas.add(new Pessoa("Andre Fcamara", listaDep));
		pessoas.add(new Pessoa("Gullit", listaDep));
		pessoas.add(new Pessoa("Maria Augusta", listaDep));
		pessoas.add(new Pessoa("Alexandre", listaDep));
		pessoas.add(new Pessoa("Danilo", listaDep));
		pessoas.add(new Pessoa("Andreia", listaDep));
		pessoas.add(new Pessoa("Ren�", listaDep));
		return pessoas;
	}
	
	private static List<Dependentes> listaDependentes() {
		List<Dependentes> listaDep = new ArrayList<>();
		
		listaDep.add(new Dependentes("Ana"));
		listaDep.add(new Dependentes("Kenia"));
		listaDep.add(new Dependentes("Claudia"));
		listaDep.add(new Dependentes("Bruna"));
		listaDep.add(new Dependentes("Antonia"));
		listaDep.add(new Dependentes("Fernanda"));
		listaDep.add(new Dependentes("Ana Carolina"));
		
		return listaDep;
	}
	
	private static List<Pessoa> listaPessoas() {
		List<Pessoa> pessoas = new ArrayList<>();
		
		pessoas.add(new Pessoa("Andre"));		
		pessoas.add(new Pessoa("Douglas"));
		pessoas.add(new Pessoa("Andre Fcamara"));
		pessoas.add(new Pessoa("Gullit"));
		pessoas.add(new Pessoa("Maria Augusta"));
		pessoas.add(new Pessoa("Alexandre"));
		pessoas.add(new Pessoa("Danilo"));
		pessoas.add(new Pessoa("Andreia"));
		pessoas.add(new Pessoa("Ren�"));
		
		return pessoas;
	}
}
