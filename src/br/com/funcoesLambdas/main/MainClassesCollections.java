package br.com.funcoesLambdas.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import br.com.funcoesLambdas.models.Pessoa;

public class MainClassesCollections {
	public static void main(String[] args) {
		// Carga de Dados
		List<Pessoa> pessoas = novaLista();

		System.out.println("Lista");
		System.out.println("");
		pessoas.forEach(p -> System.out.println(p.toString()));
		System.out.println("----------------------------------------");
		// Ordenar
		pessoas = novaLista();
		System.out.println("ordenar");
		System.out.println("");
		Collections.sort(pessoas, (Pessoa p1, Pessoa p2) -> p1.getNome().compareTo(p2.getNome()));
		pessoas.forEach(p -> System.out.println(p.getNome()));
		System.out.println("----------------------------------------");
		// Filtro equals
		pessoas = novaLista();
		System.out.println("Filtrar equals Douglas  equivale ao Sql (campo) = 'Douglas'");
		System.out.println("");
		pessoas.stream()
			.filter(s -> s.getNome().equals("Douglas"))
			.collect(Collectors.toList())
			.forEach(p -> System.out.println(p.getNome()));
		System.out.println("----------------------------------------");
		// Filtro startsWith
		pessoas = novaLista();
		System.out.println("Filtrar startsWith A equivale ao Sql campo like 'A%'");
		System.out.println("");
		pessoas.stream()
			.filter(s -> s.getNome().startsWith("A"))
			.collect(Collectors.toList())
			.forEach(p -> System.out.println(p.getNome()));
		System.out.println("----------------------------------------");

		// Filtro endsWith
		pessoas = novaLista();
		System.out.println("Filtrar endsWith a equivale ao Sql campo like '%a'");
		System.out.println("");
		pessoas.stream()			
			.filter(s -> s.getNome().endsWith("a"))
			.collect(Collectors.toList())
			.forEach(p -> System.out.println(p.getNome()));
		System.out.println("----------------------------------------");

		// Filtro split
		pessoas = novaLista();
		System.out.println("Filtrar em Regex a equivale ao Sql campo like '%an%'");
		System.out.println("");
		pessoas.stream()			
			.filter(s -> s.getNome()
							.toUpperCase()					
							.contains("AN"))
			.collect(Collectors.toList())
			.forEach(p1 -> System.out.println(p1.getNome()));
			
		
		System.out.println("----------------------------------------");
		
		
		System.out.println();
		
		//Filtrar retornando uma nova lista (para o front, por exemplo)
		System.out.println("Filtrar pelo nome com menos que 5 letras");
		pessoas = novaLista();
		List<Pessoa> novaLista = pessoas.stream()	
				.filter(pessoa -> pessoa.getNome().length() <= 5)
				.collect(Collectors.toList());
		
		System.out.println(novaLista);
		
		System.out.println();
		
		//Ordenar as listas com o metodo default sort 
		pessoas = novaLista();
		Collections.sort(pessoas, (Pessoa p1, Pessoa p2) -> p1.getNome().compareTo(p2.getNome()));
		System.out.println(pessoas);
		
		System.out.println();
		
		//Ordenar a lista usando uma propriedade do proprio stream
		pessoas = novaLista();
		List<Pessoa> listaOrdenada = pessoas.stream()
			.sorted((p1, p2) -> p1.getNome().compareTo(p2.getNome()))
			.collect(Collectors.toList());
		
		System.out.println(listaOrdenada);
		
		System.out.println();
		
		
		//Outra maneira de ordenar
		pessoas = novaLista();
		pessoas.sort(Comparator.comparing(pessoa -> pessoa.getNome()));
		//Ou poderia ser usado method reference
		//pessoas.sort(Comparator.comparing(Pessoa::getNome));
		pessoas.forEach(System.out::println);
		

	}

	private static List<Pessoa> novaLista() {
		List<Pessoa> pessoas = new ArrayList<>();
		pessoas.add(new Pessoa("Andre"));
		pessoas.add(new Pessoa("Douglas"));
		pessoas.add(new Pessoa("Andre Fcamara"));
		pessoas.add(new Pessoa("Gullit"));
		pessoas.add(new Pessoa("Maria Augusta"));
		pessoas.add(new Pessoa("Alexandre"));
		pessoas.add(new Pessoa("Danilo"));
		pessoas.add(new Pessoa("Andreia"));
		pessoas.add(new Pessoa("Ren�"));
		return pessoas;
	}
}
