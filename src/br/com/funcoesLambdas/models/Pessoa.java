package br.com.funcoesLambdas.models;

import java.io.Serializable;
import java.util.List;

public class Pessoa implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8954723813242114666L;
	private Long id;
	private String nome;
	private static Long contador=0l;
	private List<Dependentes> listaDependentes;
	
	public Pessoa(String nome) {
		Pessoa.contador++;
		setId(Pessoa.contador);
		this.nome = nome;
	}
	public Pessoa(String nome, List<Dependentes> dependentes) {
		Pessoa.contador++;
		setId(Pessoa.contador);		
		this.nome = nome;		
		this.setListaDependentes(dependentes);
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Dependentes> getListaDependentes() {
		return listaDependentes;
	}


	public void setListaDependentes(List<Dependentes> listaDependentes) {
		this.listaDependentes = listaDependentes;
	}
	@Override
	public String toString() {
		return "Pessoa [id=" + id + ", nome=" + nome + ", listaDependentes=" + listaDependentes + "]";
	}

	
}
