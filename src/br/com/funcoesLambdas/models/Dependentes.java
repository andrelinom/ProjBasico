package br.com.funcoesLambdas.models;

import java.io.Serializable;

public class Dependentes implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7458211873102974769L;
	
	private Long id;
	private String nome;
	private static Long contador=0l;
	
	public Dependentes(String nome) {
		Dependentes.contador += 1;
		this.id = Dependentes.contador;
		this.nome=nome;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "Dependentes [id=" + id + ", nome=" + nome + "]";
	}
	

}
